<?php

function jtypo_admin_settings_form($form, &$form_state) {
  $form['jtypo_common'] = array(
    '#type' => 'fieldset',
    '#title' => t('Common settings'),
  );
  
  $form['jtypo_common']['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode'),
    '#description' => t('<span class="jtypo-debug">Hightlight</span> text being processed by Jare Typograph and add debug info to <code>title</code> attributes.'),
    '#default_value' => variable_get('jtypo_debug', 0),
  );

  $form['jtypo_tofs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled TOFs'),
  );
  $tofs = variable_get('jtypo_tofs', array());
  $tofs += jtypo_tofs_default_enabled();
  _jtypo_tofs_form($form['jtypo_tofs'], $tofs);
  
  $form['jtypo_auto'] = array(
    '#type' => 'fieldset',
    '#title' => t('Titles processing'),
    '#description' => t('Choose what titles should be automatically processed by Jare Typograph.'),
  );
  
  foreach (jtypo_title_types() as $type) {   
    $form['jtypo_auto'][$type] = array(
      '#type' => 'checkbox',
      '#title' => t($type),
      '#default_value' => variable_get('jtypo_auto_' .$type, 0),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function jtypo_admin_settings_form_submit($form, &$form_state) {
  foreach (element_children($form['jtypo_auto']) as $var) {
     variable_set('jtypo_auto_' . $var, $form_state['values'][$var]);
  }
  
  $tofs = array();
  foreach (element_children($form['jtypo_tofs']) as $var) {
    $tofs[$var] = $form_state['values'][$var];
  }
  variable_set('jtypo_tofs', $tofs);
  
  variable_set('jtypo_debug', $form_state['values']['debug']);
}