
About
-----

  JTypo module makes your texts look nicer, applying configurable
  set of typography rules (45 rules altogether grouped into 6 
  sets called "tof"s).
  
  It provides:
  
      * Input filter 'Jare Typograph' which can be used with any 
        Text Format.
        
      * Theme-layer filters which applies to different titles 
        and captions of pages, nodes, blocks, panel panes, 
        menu links, menu local tasks, form_labels and form_fields.
        
  Features:
  
      * With the aid of source_popup module you can get clear view
        of what text was "jtyped", and how it was - via Shift+click.
        Give it a try, it is handy.

Installation
------------
  
  Nothing special, just unpack into /sites/all/modules and you're done.
  
Configuration
-------------

  To make use of input filter, visit admin/config/content/formats page
  and enable the filter Jare Typograph. Below in vertical tabs you can 
  configure which tofs should be used.
  
  To make use of preprocess title filters visit admin/config/content/jtypo
  and enable things you want to process and tofs you need.
  
Jare Typograph
--------------
  Jare Typograph PHP library deals with text transformations,
  to make texts look nice. The library was initially developed
  for Russian texts formatting, but might be useful for other 
  languages either.
  
  It has many rules, grouped into so called "tof"s. Each tof 
  is dealing with a subset of text transformations. Here is a 
  brief listing of implemented tofs:
  
      * Dash - adding correct long, short and hypens
      
      * Numbers - formatting nubmers, fractionals
      
      * Pucntmark - fixes brackets, adding commas, placing 
        punctiuation marks
        
      * Quote - quotes handling
      
      * Space - rules the usage of whitepaces and non-breaking
        spaces (&nbsp;).
        
      * Etc - extra rules which don't fit into other categories:
        replacing (c) with &copy;, (tm) with &tm; and etc.
  
  The library can be easily extended for it was designed to 
  be extensible. For example, new rules can be added in run-time,
  similary - any existing rule can be tweaked in run-time too.
        
  The latest version of the library was released in 2009 by 
  E.Muravyjov Studio. Unfortunately the library download page 
  is not available anymore, neither license text could be seen: 
  http://emuravjev.ru/works/tg/eula/
  Also, I cound't contact the author of the library to get permissions 
  to include the library in the distribution of this module.
  From the other hand, I don't believe I violate something, since 
  the library was freely distributable.
  
  The source inclulded in this distribution is a copy, which I got
  via E-mail from HIgor1968. The original discussion thread is located 
  at http://www.drupal.ru/node/32692
 
  I modified the source a bit to get rid off PHP warnings. Apart from 
  that, it is the latest original version of Jare Typograph.
  


   
       