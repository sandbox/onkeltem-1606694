(function ($) {

  function escapeHtml(unsafe) {
  return unsafe
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
  }

  Drupal.behaviors.jtypoDebug = {
    attach: function(context, settings) {
      $('.jtypo-debug1').bind("click", function(e) {
        if (e.shiftKey) {
          var that = this;
          $(this).clone().dialog({
            dialogClass: 'jtypo-debug-dialog',
            autoOpen: true,
            position: [e.pageX, e.pageY],
            height: 350,
            width: '70%',
            open: function(event, ui) {
              $(that).addClass('exposed');
              $(event.target).removeClass('jtypo-debug');
              $(event.target).html(escapeHtml($(event.target).html()));
              $(event.target).wrapInner('<div class="jtypo-debug-wrapper"/>').
                children('.jtypo-debug-wrapper').
                wrapInner('<textarea class="jtypo-debug-code"/>');
            },
            close: function(event, ui) {
              $(that).removeClass('exposed');
            }
          });
          e.stopPropagation();
          e.preventDefault();
        }
      });
      /*
      $('.jtypo-debug')
        .hover(
          function (e) {
            if (e.shiftKey) {
              $(this).addClass('hover');
            }
          },
          function (e) {
            $(this).removeClass('hover');
          }
        )
        .mousemove(
          function (e) {
            if (e.shiftKey && !$(this).hasClass('hover')) {
              $(this).addClass('hover');
            }
          }
        ) 
        .keydown(
          function (e) {
            a = 1;
          }
        );
        */
      /*   
      $(context)
        .keydown(
          function (e) {
            if (e.keyCode == 16) {
              $('.jtypo-debug').addClass('editable');
            }
          }
        )
        .keyup(
          function (e) {
            if (e.keyCode == 16) {
              $('.jtypo-debug').removeClass('editable');
            }
          }
        )
        .hover(
          function (e) {
            if (!e.shiftKey) {
              $('.jtypo-debug').removeClass('editable');
            }
          }
        );*/
    }
  }

})(jQuery);
